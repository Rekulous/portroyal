# [port royal]

[https://deepdive.gg/portroyal](https://deepdive.gg/portroyal)

This is a set of markdown files used to generate the [port royal] page with the tool [ssg6](https://www.romanzolotarev.com/ssg.html) by Roman Zolotarev.

## Ways to contribute

- By adding new sections that didn't exist yet.

- Fix errors in pre-existing sections or add improvements.

## Rules for submission

- Make sure to not post any links that contain a virus or that are malicious in any other way.

- Do not submit duplicates or dead links. 


