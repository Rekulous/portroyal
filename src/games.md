## [games]

###[Info and Tools]
- [What are scene releases or Cracks?](https://www.reddit.com/r/CrackWatch/comments/92uz49/the_warez_scene_how_it_works/)
- [What are Torrents?](https://en.wikipedia.org/wiki/BitTorrent)
- [/r/CrackWatch](https://reddit.com/r/CrackWatch/) - New video game crack releases are posted here
- [PreDB](https://predb.ovh) - PreDB sites are for releases of new cracks or scene releases of games that do not provide downloads or links to pirated stuff at all. Only for information and research purposes. Other sites that are for PreDB rather than Predb.ovh are: https://predb.org and https://predb.pw. 
- [xREL](https://xrel.to) - xREL is just like PreDB, Pre-database scene releases for games just in a different look and interface. 
- [SmartSteamEmu](https://github.com/MAXBURAOT/SmartSteamEmu) - Used to emulate steam without having it installed or open! Not for putting in repacks or anything else, so its more of personal use if that's what you can call it.
- [GoldBerg Emulator](https://gitlab.com/Mr_Goldberg/goldberg_emulator) - A Steam emulator that emulates steam online features. Lets you play games that use the steam multiplayer APIs on a LAN without steam or an internet connection. Contains files that are like the Steam API DLL files so they are good for putting in repacks or pre-installed games. Probably the best emulator by far yet.
- [Steamless](https://github.com/atom0s/Steamless) - Steamless is a DRM remover of the SteamStub variants. 
- [CreamAPI](https://cs.rin.ru/forum/viewtopic.php?t=70576) "A Legit DLC Unlocker" for Steam. There are other dlc unlockers similar to CreamAPI for Epic Games, Origin, etc.
- [Auto-CreamAPI](https://cs.rin.ru/forum/viewtopic.php?p=2013521#p2013521) - Automatically set your game up for CreamAPI.
- [Redump.org](http://redump.org/) - Disc preservation database and internet community dedicated to collecting precise and accurate information about every video game ever released on optical media of any system.

###[Direct Download Games]
- [cs.rin.ru](https://cs.rin.ru/) - Popular gaming piracy forum that provides non-scene release downloads for mostly Steam games. They also have many information and tutorial posts about cracking. Has one of the best quality of games overall.
- [GamesDrive](https://gamesdrive.co) - Forum for downloading repacks, scene releases, DOX, repacks. etc. from the community and scene releases. No signup is required. 
- [Torrminatorr](https://torrminatorr.com) - Forum for downloading scene releases, gog-rips, 0Day, etc. Provides Linux game Downloads as well. 
- [GOG-Games](https://gog-games.com) - GOG Rips from the website gog.com, all downloads are untouched since Games on GOG do not have DRM.
- [GOD scraped URLs](https://drive.google.com/file/d/17MB0gCcCMr3QqE_CgJkaxmdXtZk61TdZ/view) All DDL links for games listed on the now-dead GoodOldDownloads site.
- [GLOAD](https://gload.cc) - German DDL site for games.
- [SCNLOG](https://scnlog.me) - DDL site that provides untouched downloads for all scene releases rather than torrenting them. Provides Linux, Windows, and Mac games. 
- [RLSBB](https://rlsbb.ru) - DDL site similar to SCNLOG that provides untouched downloads for all scene releases of games. Provides Linux, Windows, and Mac games. 
- [SteamUnlocked](https://steamunlocked.net) - DDL Pre-installed games without having to install or uninstall them. Download, Unpack, Play, Delete. Its that simple and is recommended for beginners!
- [CrackHub](https://crackhub.site) - DDL games from Scene Groups, P2P Crackers, and Repackers (mostly fitgirl) at fast download speeds. Mostly has newly cracked and repacked games rather than older cracks and repacks. 
- [Online-Fix.me](https://online-fix.me) - Russian site that Provides Techniques and Downloads for Online Games!
- [Ovagames](https://ovagames.com) - Download Cracks of Games, new to old. 
- [MyAbandonware](https://myabandonware.com) - Provides Downloads for Abandonware and Retro Games. Doesn't provide downloads for every single old game since they are only supposed to post abandonware, a game or software that is abandoned and not updated anymore. Most commonly obscure and forgotten. They are not sold in official game stores like Steam or GOG, so therefore, they are free to download now as they cannot be obtained anywhere else online.
 
###[Repacks]
- [FitGirl Repacks](http://fitgirl-repacks.site/) Popular DDL and torrent site for game repacks
- [Masquerade](https://masquerade.site) - Popular DDL Repack site for repacks.
- [Xatab Repacks](https://xatab-repack.com/) - Russian game repacker, primarily torrents. 
- [Darck](https://darckrepacks.com/) - Forum site for Darck and Community repacks. Small repackers post there and so do big ones.
- [ElAmigos](https://www.elamigos.site/) - Links to unnoficial ElAmigos Games and Repacks. ElAmigos doesn't have an official site. The only official thing from them is a pastebin for their games.
- [CPG Repacks](http://cpgrepacks.site/) - Anime and Japanese game repacks
- [Gnarly](https://gnarly-repacks.site) - Emulator, Retro, Classic, and Old Game repacks because old games were just awesome. 
- [Le Fishe](https://lefishe-repacks.rf.gd) - Small repacker that repacks all sorts of games.
- [R.G Mechanics](https://s1.rg-mechanics.org/) - Torrent Repacks from R.G Mechanics. Find them on 1337x or use the site provided. 
- [M4CKD0GE](https://m4ckd0ge-repacks.me) 
- [DODI](http://dodi-repacks.site/) - Very well compressed repacks for big games kind of like FitGirl
- MR DJ - Torrents
- R.G. Revenants - Torrents
- R.G. Catalyst - Torrents
- ZAZIX - Torrents
- [KaosKrew](https://kaoskrew.org/) - Pre-Cracked Repacks 
- [Kapital Sin](http://www.kapitalsin.com/forum/) - Spanish Forum site for Game Repacks
- [SC00T3R Repacks](https://scooter-repacks.site/) - Repacks for Online-Fixes and Games

###[roms]
- [Romsmania](https://romsmania.cc/) - Good ROMs collection with a decent UI.
- [Doperoms](https://www.doperoms.com/) - Huge collection with over 170,000 ROM files. PS3 included.
- [Vimm's Lair](https://vimm.net/?p=vault) - Large collection of ROMs
- [Romulation.net](https://www.romulation.net/) - Collection of ~28,000 console game ROMs
- [The Eye ROMs](http://the-eye.eu/public/rom/) - Open directory of ROMs from The Eye
- [Old Games Finder](http://www.oldgamesfinder.com/) - Old Games Finder is an automated old game search engine.](avoid ISO Zone links, as that site is dead)
- [The ROM Depot](https://theromdepot.com/roms/) - About 3TB of ROMs. You may need a VPN.
- [Emulator.Games](https://emulator.games/) - Download or play ROMs on your PC, Mobile, Mac, iOS and Android devices.
- ["A simple script for easily downloading emulator.games roms"](https://www.reddit.com/r/Piracy/comments/aytutr/a_simple_script_for_easily_downloading/) - Reddit guide and userscript created by /u/estel_smith to allow you to easily download ROMs from Emulator.Games.
- [3DSISO](http://www.3dsiso.com/) Nintendo 3DS ROMs downloads forum.
- [3DSCIA.com](https://www.3dscia.com/) DDL links for 3DS CIA files.
- [Ziperto](https://www.ziperto.com/) - DDL link site primarily for Nintendo games.
- [Edge Emulation](https://edgeemu.net/) - A very clean looking website that has all the ROMs up until the Sega Dreamcast.
- [CDRomance](https://cdromance.com/) - PSP, PSX, PS2, Gameboy, NDS, SNES, Dreamcast, and Gamecube ROMs and ISOs.
