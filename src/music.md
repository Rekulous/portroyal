##[music]

###[streaming]
- [Spotify](https://file.dlandroid.com/index.php?id=spotify-premium-apk-mod) Cracked Spotify for Android.
- [Freezer](https://freezer.life/) The definitive Deezer streaming application. Works on Android, Windows and Linux.
- [NewPipe](https://newpipe.net/) A FOSS streaming application that supports both Youtube and Soundcloud. No Ads.
- [Muxiv Music](https://muxiv.com/) Stream 45 million songs on all your devices, online or offline. Primarily Chinese content.
- [Hikarinoakariost](https://hikarinoakariost.info/) Site with Japanese music
- [mp3Clan](http://mp3guild.com/) Free music streaming
- [GoSong](https://gosong.unblocked.gdn/) Streamable MP3s
- [MP3Juices](https://mp3juices.unblocked.gdn/) MP3 search engine tool which uses YouTube
- [mp3.li](http://mp3li.unblckd.club/) Another MP3 streaming site
- [SongsPK](https://songs-pk.in/) Mainly for downloading Bollywood songs. Domain changes frequently.
- [datmusic](https://datmusic.xyz/) Search engine with a clean UI for streaming music in your browser
- [MusicPleer](https://musicpleer.la/) Another music streaming site with a decent search engine
- [slider.kz](http://slider.kz/) Quirky and fast music streaming site

###[downloading]
- [Youtube-DL](https://youtube-dl.org/) The free and open source Youtube downloading tool. Works with Youtube, Soundcloud, Bandcamp etc.
- [Lucid Archive](http://lucidarchive.com/) Giant archive of Vaporwave albums.
- [Soulseek](http://www.soulseekqt.net/news/) Soulseek is an ad-free, spyware free, just plain free file-sharing network for Windows, Mac, and Linux.
- [irs](https://github.com/kepoorhampond/irs) A music downloader that understands your metadata needs.
- [SMLoadr](https://git.fuwafuwa.moe/SMLoadrDevs/SMLoadr) A streaming music downloader.
- [Deezloader Remaster](https://www.reddit.com/r/DeezloadersIsBack/comments/9n3pf1/deezloader_alpha_latest_version_download10102018/) Tool for downloading music from Deezer
- [Deezloader Remix](https://notabug.org/RemixDevs/DeezloaderRemix) Another program with the same purpose, both based on the original, now defunct Deezloader.
- [/r/DeezloaderIsBack](https://www.reddit.com/r/DeezloadersIsBack) Community supporting Deezloader
- [Deemix](https://download.deemix.app/pyweb/) Another program with the same purpose.
- [/r/deemix](https://www.reddit.com/r/deemix) Community supporting Deemix
- [New Album Releases](http://newalbumreleases.net/) Premium DDL links for full albums
- [SittingOnClouds](https://www.sittingonclouds.ru/) Site full with Video Game and Anime Soundtracks. Has FLAC as well. 
- [Kingdom Leaks](https://kingdom-leaks.com/) DDL links for album leaks
- [KHInsider](https://downloads.khinsider.com/) Site collecting soundtracks, mostly MP3, some FLAC, OGG or M4A.
- [VGMLoader](https://github.com/TheLastZombie/VGMLoader) Tool for bulk downloading from KHInsider.
- [Free MPS Download.net](https://free-mp3-download.net/) Search engine with streamable samples and download links
- [chimera](https://notabug.org/Aesir/chimera) Multiple source terminal-based music downloader with audio search engine
