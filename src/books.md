##[books & comics]


### [ebooks]
- [BookStack](https://www.bookstackapp.com/) BookStack is a simple, self-hosted, easy-to-use platform for organizing and storing information.
- [Ubooquity](http://vaemendis.net/ubooquity/) Ubooquity is a free home server for your comics and ebooks library
- [COPS](https://github.com/seblucas/cops) Calibre OPDS (and HTML) PHP Server : web-based light alternative to Calibre content server / Calibre2OPDS to serve ebooks (epub, mobi, pdf, etc.)
- [b-ok](http://b-ok.xyz/) Free ebook library
- [The idiot-proof guide to downloading ebooks off IRC](https://www.reddit.com/r/Piracy/comments/2oftbu/guide_the_idiot_proof_guide_to_downloading_ebooks/) Posted by /u/Servaplur
- [Guide to Copy Kindle Content to PDF using Calibre](https://www.reddit.com/r/Piracy/comments/bm837l/guide_to_copy_kindle_content_to_pdf_using_calibre/)
- [Apprentice Alf's Blog](https://apprenticealf.wordpress.com/) Everything you ever wanted to know about DRM and ebooks but were afraid to ask.
- [Calibre](https://calibre-ebook.com/) :star2: ebook management tool
- [Calibre-Web](https://github.com/janeczku/calibre-web) Web app for browsing, reading and downloading eBooks stored in a Calibre database
- [Custom Search Engine](https://cse.google.com/cse?cx=000661023013169144559:a1-kkiboeco) A Google custom search engine specifically for ebooks
- [Exploring over 1,800 Calibre ebook servers](https://blog.chrisbonk.ca/2018/12/knowledge-is-power-exploring-over-1800.html?m=1) Blog post detailing how to use Shodan to find Calibre ebook servers
- [DeDRM_tools](https://github.com/apprenticeharper/DeDRM_tools) DeDRM tools for ebooks.
- [ReadAnyBook](https://readanybook.com/) Free online reading.
- [PDFdrive](http://pdfdrive.com) PDF Drive is your search engine for PDF files. No annoying ads, no download limits.
- [Memory of the world](http://library.memoryoftheworld.org) A new website containing a lot of books.

### [magazines]
- [PDF Giant](http://pdf-giant.com/) Various categories of downloadable PDFs
- [MagazineLib](https://magazinelib.com/) Free PDF and interactive e-magazines

### [academic papers and material]
- [LibGen](https://libgen.fun/) search engine for articles and books on various topics, which allows free access to content that is otherwise paywalled or not digitized elsewhere
- [Sci-Hub](https://sci-hub.se/) the first pirate website in the world to provide mass and public access to tens of millions of research papers
- [BookSC](http://booksc.org/) The world's largest scientific articles store. 50,000,000+ articles for free.
- [Academic Torrents](http://academictorrents.com/) A Community-Maintained Distributed Repository for researchers, by researchers. Making 32.66TB of research data available!

### [textbooks]
- [All IT eBooks](http://www.allitebooks.com/) A big database of free, direct links for IT and programming ebooks
- [it-ebooks](http://it-ebooks.info) Large selection of free and open-source IT ebooks
- [PDF/Ebook trackers for college textbooks](https://www.reddit.com/r/trackers/comments/hrgmv/tracker_with_pdfsebooks_of_college_textbooks/c1xrq44/) Old-but-still-useful list of ebook/textbook trackers, DDL sites, and IRC communities
- [How to "rent" your textbooks for free from Amazon](https://www.reddit.com/r/Piracy/comments/3ma9qe/guide_how_to_rent_your_textbooks_for_free_from/) "Going to college? Living off top ramen for dinner? Let me show you have to "rent" your textbooks for free & for life!"
- [Guide for Finding Textbooks](https://www.reddit.com/r/Piracy/comments/3i9y7n/guide_for_finding_textbooks/) Extensive tutorial by /u/Amosqu
- [forcoder](https://forcoder.su/) Ebooks & Elearning For Programming

### [courses and tutorials]
- [CourseClub](https://courseclub.me/) Download courses from (Lynda, Pluralsight, CBG Nuggets, etc)
- [FreeCourseSite](https://freecoursesite.com/) Mostly highest rated udemy courses torrent
- [FreeTutorials.eu](https://www.freetutorials.eu/) Lots of Udemy courses for free; Has Adblock detector
- [Gigacourse](https://gigacourse.com/)
- [Desire Course](https://desirecourse.net/)
- [GFXDomain.net Tutorials board](http://forum.gfxdomain.net/forums/others-tutorials.42/) Forum with free tutorials for graphic design, mostly via premium file hosts but some torrents
- [tpget](https://github.com/0x6a73/tpget) Tutorialspoint downloader
- [udemy-downloader-gui](https://github.com/FaisalUmair/udemy-downloader-gui) A cross-platform (Windows, Mac, Linux) desktop application for downloading Udemy Courses.
- [tut4dl](https://tut4dl.com/) Download tutorial and training courses from many paid MOOCs, with categories ranging from Cuisine to Cryptography.

### [audiobooks]
- [AudioBook Bay](http://audiobookbay.nl/) Download unabridged audiobooks for free or share your audiobooks, safe, fast and high quality
- [AAXtoMP3](https://github.com/KrumpetPirate/AAXtoMP3) Convert Audible's .aax filetype to MP3, FLAC, M4A, or OPUS
- [Booksonic](http://booksonic.org/) Booksonic is a server and an app for streaming your audiobooks to any pc or android phone.
- [The Eye /public/AudioBooks](http://the-eye.eu/public/AudioBooks/) A few publicly accessible audiobooks hosted by The Eye
- [AudioBooks.Cloud](https://audiobooks.cloud/) DDL links for lots of audiobooks.
- [Tokybook](https://tokybook.com/) Free audiobook streaming site.

### [comicbooks]
- [Kindle Comic Converter](https://kcc.iosphe.re/) Comic and manga converter for ebook readers
- [readcomiconline.to](https://readcomiconline.to/) Manga and comics uploaded daily
- [Readcomicbooksonline](https://readcomicbooksonline.org/) Tends to Error 520 occasionally
- [Comic Extra](https://www.comicextra.com/) Daily comic uploads, clean UI
- [GetComics](https://getcomics.info/) GetComics started as an alternative place to get downloaded comic files, particularly US-based comics published by DC and Marvel.
- [Gazee!](https://hub.docker.com/r/linuxserver/gazee/) A WebApp Comic Reader for your favorite digital comics. Reach and read your comic library from any web-connected device with a modern web browser.
- [Comix-Load](https://comix-load.in/) DDL links for comic books and manga in English and German.
- [Omnibus](https://github.com/fireshaper/Omnibus) Search for and download comics that are added to GetComics.info easily

### [manga]
- [MangaDex](https://www.mangadex.org/) MangaDex is an online manga reader that caters to all languages.
- [/r/manga](https://www.reddit.com/r/manga) Everything and anything manga! (manhwa is okay too!)
- [Madokami](https://manga.madokami.al/) Requires sign-up (currently closed), see mirrors below.
- [Madokami 0-E](https://archive.org/download/Madokami.Manga.0-E) Download manga titles named 0 to E.
- [Madokami F-K](https://archive.org/download/Madokami.Manga.F-K) Download manga titles named F to K.
- [Madokami L-Q](https://archive.org/download/Madokami.Manga.L-Q) Download manga titles named L to Q.
- [Madokami R-Z](https://archive.org/download/Madokami.Manga.R-Z) Download manga titles named R to Z.
- [Madokami novels, raws and artbooks](https://archive.org/download/Madokami.NotManga) Download novels, manga raws and artbooks.
- [Tachiyomi](https://tachiyomi.org/) Free and open source manga reader for Android.
- [MangaZone](http://mangazoneapp.com/) A manga reader app.
- [NineAnime](https://www.nineanime.com/) Updated/Active Manga Site
- [Free Manga Downloader (FMD)](https://github.com/fmd-project-team/FMD) A manga download manager and reader that supports downloading from various websites.
- [HakuNeko](https://github.com/manga-download/hakuneko) A cross-platform downloader for manga and anime from +400 websites. Manga and anime in multiple languages and formats (cbz, a folder with images, epub).
