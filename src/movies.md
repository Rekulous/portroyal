##[movies & tv shows]

###[streaming]
- [MovieStreamingSites](https://www.reddit.com/r/MovieStreamingSites/) Reddit, random streaming sites
- [HD MultiredditHD](https://www.reddit.com/user/nbatman/m/streaming2/) Alternate subreddit curated by /u/nbatman
- [Best Free Streaming](https://www.bestfreestreaming.com/) Site that rates streaming services
- [StreamCR](https://scr.cr/) Clean design, very nice speeds, a large variety of films and series, HD server, Popular Site
- [YMovies](https://ymovies.tv/) Unique design, HD server with additional hosts, nice speeds, YIFY and other releases (+ torrents)
- [ HDO](https://hdo.to/) Unique design, HD server with additional hosts, also country-specific films/series
- [ M4UFree.TV](http://m4ufree.tv/) Unique design, HD server with backup and additional hosts
- [ Movie123](http://movie123.club/) Unique design, HD server with Backup and additional hosts
- [ LookMovie](https://lookmovie.ag/) Unique design, HD server, very nice speeds (offers auto quality)
- [ AZMovies](https://azmovies.xyz/) Unique design, HD server with additional hosts, also on Reddit
- [ Streamlord](http://www.streamlord.com/) Unique design, HD server (subtitles)
- [ FlixGo](https://flixgo.net/) Unique design, HD server, very nice speeds
- [Solarmovie](https://solarmoviez.ru/solar.html) Basic streaming site layout, HD server with additional hosts, Popular Site
- [123Movies](https://123movies.website/) Basic streaming site layout, HD server with additional hosts. Previously HDFlix.
- [Yes! Movies](https://yesmovies.to/) Basic streaming site layout, HD server with additional hosts
- [Spacemov](http://spacemov.io/) Basic streaming site layout, HD server, only certain films have additional hosts
- [HDOnline](https://www1.hdonline.eu/) Basic streaming site layout, HD server with additional hosts
- [#1 Movies Website](https://www1.1movies.is/)  Basic streaming site layout, HD server with additional hosts
- [CMoviesHD](https://www2.cmovieshd.bz/) Basic streaming site layout, HD server with additional hosts
- [Vidcloud](https://vidcloud.icu/) Basic streaming site layout, HD server with additional hosts
- [Series9](https://www2.series9.io/) Unique design, HD server with additional hosts
- [Soap2day](https://www.soap2day.com/) Unique design, very nice speeds, HD server with subtitles.
- [Best-movies.watch](https://best-movies.watch/) Unique design, more than 19000 available

###[anime]

- [Nyaa](https://nyaa.si/) BitTorrent software for cats (Repo <https://github.com/nyaadevs/nyaa)
- [Hi10 Anime](https://hi10anime.com/) High-Quality 10-bit Anime Encodes
- [Anime Kaizoku](https://animekaizoku.com/) Up to 1080p DDL links, mostly Google Drive
- [Anime Kayo](https://animekayo.com/) Up to 1080p DDL links, mostly Google Drive
- [/r/animepiracy](https://www.reddit.com/r/animepiracy) This sub is about streaming and torrent websites for anime.
- [/r/animepiracy wiki](https://www.reddit.com/r/animepiracy/wiki/index) Lists for sourcing Anime streaming sites, manga sites, and more
- [9Anime](https://9anime.to/) Watch anime online. English anime, dubbed, subbed.
- [All-animes](https://all-animes.com/) Watch Online Anime In HD English Subbed, Dubbed.
- [GoGo Anime](https://www3.gogoanime.in/) Popular website for watching anime
- [AniLinkz](https://anilinkz.to/) Large database of streaming anime episodes.
- [NyaaPantsu](https://nyaa.pantsu.cat/) Primarily Anime torrents but includes an open directory of DDL links too.
- [Alternatives to Kiss websites](https://www.reddit.com/r/KissCartoon/wiki/alternatives) /r/KissCartoon wiki page with lots of anime sites
- [anime-sharing](http://www.anime-sharing.com/forum/) Forum for sharing anime
- [AniDex](https://anidex.info/) Torrent tracker and indexer, primarily for English fansub groups of anime
- [animeEncodes](https://www.animencodes.com/)
- [HorribleSubs](https://horriblesubs.info/) Download anime via torrent files, magnet links, XDCC, and premium link hosts.
- [Anime Twist](https://twist.moe/) An anime direct streaming site with a decent UI and video player
- [AnimeOut](https://www.animeout.xyz/) Over 1000's of Encoded Anime with DDL links.
- [Kissanime.ac](https://kissanime.ac/) Large cartoon collection, uses RapidVideo/Openload
- [Anime8](https://anime8.me/) Basic streaming site layout, large collection of anime shows
- [4anime](https://4anime.to/) A relatively new site the might become the new Masterani.me. Clean interface.
- [AnimeRam](https://ww2.animeram.cc/) A streaming website for dubbed/subbed anime.
- [animepahe](https://animepahe.com/) A minimilistic anime streaming/download website, subs only.
